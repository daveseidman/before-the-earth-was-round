import React, { useState, useRef } from 'react';
import { Canvas } from '@react-three/fiber';
import audioFile from './assets/audio/before-the-earth-was-round.mp3';

function App() {
  const audioRef = useRef();
  const analyserRef = useRef();
  const [audioContext, setAudioContext] = useState(null);
  const [analyserData, setAnalyserData] = useState('');
  const [playing, setPlaying] = useState(false);

  const startContext = () => {
    const context = new (window.AudioContext || window.webkitAudioContext)();
    setAudioContext(context);

    const analyser = context.createAnalyser();
    analyser.fftSize = 32; // Adjust as needed
    analyserRef.current = analyser;

    const audioElement = audioRef.current;
    const source = context.createMediaElementSource(audioElement);
    source.connect(analyser);
    analyser.connect(context.destination);
  };

  const updateAnalyser = () => {
    const analyser = analyserRef.current;
    const bufferLength = analyser.frequencyBinCount;
    const dataArray = new Uint8Array(bufferLength);
    analyser.getByteFrequencyData(dataArray);
    console.log(dataArray);
    setAnalyserData(dataArray.join(','));

    // Continue updating on each frame
    if (!audioRef.current.paused) requestAnimationFrame(updateAnalyser);
  };

  const handlePlayClick = () => {
    if (audioContext.state === 'suspended') {
      audioContext.resume();
    }
    if (!playing) {
      audioRef.current.play().then(() => {
        setPlaying(true);
        updateAnalyser();
      });
    } else {
      audioRef.current.pause();
      setPlaying(false);
    }
  };

  return (
    <div>
      <button
        type="button"
        onClick={startContext}
        disabled={audioContext !== null}
      >
        Start
      </button>
      <button
        type="button"
        onClick={handlePlayClick}
      >
        {playing ? 'Pause' : 'Play'}
      </button>

      <audio ref={audioRef} src={audioFile} />
      <div>
        {analyserData}
      </div>

      <Canvas />
    </div>
  );
}

export default App;
